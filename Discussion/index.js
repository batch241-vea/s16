
////	Arithmetic Operators

let x = 5555;
let y = 1111;

let sum = x + y;
console.log("Result of addition operator: " + sum);

let difference = x - y;
console.log("Result of subtraction operator: " + difference);

let product = x * y;
console.log("Result of multiplication operator: " + product);

let quotient = x / y;
console.log("Result of division operator: " + quotient);

let remainder = x % y;
console.log("Result of modulo operator: " + remainder);

//// ASSIGNMENT OPERATOR

// Basic Assignment Operator(=)
let assignmentNumber = 8;

// Addition Assignment Operator
assignmentNumber += 2;
console.log("Result of the addition assignment operator: " + assignmentNumber); //10

// Subtraction Assignment Operator
assignmentNumber -= 2;
console.log("Result of the subtraction assignment operator: " + assignmentNumber); //8

// Multiplication Assignment Operator
assignmentNumber *= 2;
console.log("Result of the multiplication assignment operator: " + assignmentNumber); //16

// Division Assignment Operator
assignmentNumber /= 2;
console.log("Result of the division assignment operator: " + assignmentNumber); //8


//// Multiple Operators and Parenthesis
// follows PEMDAS rule

let mdas = 1 + 2 -3 * 4 / 5;
console.log("Result of pemdas operation: " + mdas); //0.6

let pemdas = 1 + (2 - 3) * (4 / 5);
console.log("Result of pemdas operation: " + pemdas); //0.2

// prioritize modulo
let sample = 2 + 9 % 8;
console.log(sample);

//// Increment and Decrement

// INCREMENT
// Pre-increment
// the value of variable is changing
let z = 1;
let increment = ++z;
console.log("Result of the pre increment: " + increment); //2
console.log("Result of the pre increment: " + z); //2

// Post Increment
// the value of variable is unchanging
let w = 3;
let postIncremenet = w++;
console.log("Result of the post increment: " + postIncremenet); //1
console.log("Result of the post increment: " + w);

/*
	Pre increment - adds 1 first before reading the value
	Post incremenet - reads the value first before adding 1
*/

// DECREMENT
//Pre decrement
let a = 5;
let preDecrement = --a;
console.log("Result of the pre decrement: " + preDecrement);
console.log("Result of the pre decrement: " + a);

//Post decrement
let b = 5;
let postDecrement = b--;
console.log("Result of the post decrement: " + postDecrement);
console.log("Result of the post decrement: " + b);
/*
	Pre decrement - substract 1 first before reading the value
	Post decrement - reads the value first before substracting 1
*/

//// TYPE COERCION
/*
	this is the automatic or implicit conversion of values from one data type to another
*/

let numA = "10"; //String
let numB = 12; // Number
/*
	Adding/concatenating a string/number will result as a string	
*/
let coercion = numA + numB;
console.log(coercion); //1012
console.log(typeof coercion); //String

let coercion1 = numA - numB;
console.log(coercion1); 
console.log(typeof coercion1); 

// Non-coercion
let numC = 16;
let numD = 14;
let nonCoercion = numC + numD;
console.log(nonCoercion);
console.log(typeof nonCoercion);

//Addition of Number and Boolean
/*
	value of boolean true = 1
	value of boolean false = 0
*/
let numE = true + 1;
console.log(numE); //2
console.log(typeof numE);

let numF = false + 1;
console.log(numF); //2
console.log(typeof numF);

// COMPARISON OPERATOR
let juan = "juan";

//equality operator (==)
/*
	- checks whether the operands are equal or have the same content
	- attempts to convert and compare operands of different data types
	- returns a boolean value(true/false)
	*/

console.log(1 == 1); //t
console.log(1 == 2); //f
console.log(1 == "1"); //t
console.log(1 == true); //t
console.log(juan == "juan"); //t

//inequality operator(!=)
/*
	- checks whether the operands are not equal or have different content
	- attempts to convert and compare operands of different data types
*/

console.log(1 != 1); //F
console.log(1 != 2); //T
console.log(1 != "1"); //F
console.log(1 != true); //F
console.log(juan != "juan"); //F

// Strict Equality Operator(===)
/*
	- check whether the operands are equal or have the same content
	- compares the data types of the 2 values
*/
console.log(1 === 1); //T
console.log(1 === 2); //F
console.log(1 === "1"); //F
console.log(1 === true); //F
console.log(juan === "juan"); //T

// Strict Inequality Operator (!==)
/*
	- checks whether the operands are not equal or have different content
	- compares the data types of the 2 values
*/
console.log(1 !== 1); //F
console.log(1 !== 2); //T
console.log(1 !== "1"); //T
console.log(1 !== true); //T
console.log(juan !== "juan"); //F

// Relational Operator
/*
	- returns boolean value
*/
let j = 50;
let k = 65;

//Greater than operator (>)
let isGreaterThan = j > k;
console.log(isGreaterThan);

//Less than operator(<)
let isLessThan = j < k;
console.log(isLessThan)

// Greater than or Equal Operator(>=)
let isGTorEqual = j >= k;
console.log(isGTorEqual)

// Less than or Equal Operator(>=)
let isLTorEqual = j <= k;
console.log(isLTorEqual)

let numStr = "40";
// forced coercion to change the string to a number
console.log(j > numStr);

// Logical Operators
let isLegalAge = true;
let isRegistered = false;

//Logical AND Operator (&&)
/*
	- automatically returns false if any of the operand has false, otherwise true
*/

let allRequirementsMet = isLegalAge && isRegistered;
console.log(allRequirementsMet);

// Logical OR Operator (||)
/*
	- automatically returns true if any of the operand has true, otherwise false
*/
let someRequirementsMet = isLegalAge || isRegistered;
console.log(someRequirementsMet);

// Logical NOT Operator (!)
let someRequirementsNotMet =! isRegistered;
console.log(someRequirementsNotMet) 